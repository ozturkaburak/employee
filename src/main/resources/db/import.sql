insert into public.office (name, location)
values ('XYZ Ankara Office', 'Turkey/Ankara');
insert into public.office (name, location)
values ('XYZ Istanbul Office', 'Turkey/Istanbul');

insert into public.department (name)
values ('HR');
insert into public.department (name)
values ('Engineering');
insert into public.department (name)
values ('Finance');

-- insert into public.office_departments(office_id, departments_id) values (1,1);
-- insert into public.office_departments(office_id, departments_id) values (1,2);
-- insert into public.office_departments(office_id, departments_id) values (2,1);
-- insert into public.office_departments(office_id, departments_id) values (2,2);
-- insert into public.office_departments(office_id, departments_id) values (2,3);